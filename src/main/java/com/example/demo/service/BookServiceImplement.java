package com.example.demo.service;

import com.example.demo.model.DataTransfer.BookDto;
import com.example.demo.model.DataTransfer.CategoryDto;
import com.example.demo.pagination.Pagination;
import com.example.demo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImplement implements BookService {
    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookDto> getAll(Pagination pagination) {
        return bookRepository.getAllData(pagination);
    }

//    @Override
//    public List<BookDto> findAll() {
//        return bookRepository.findAll();
//    }

    @Override
    public BookDto insert(BookDto dto) {
        boolean isInserted = bookRepository.insert(dto);
        if (isInserted) {
            return dto;
        } else {
            return null;
        }
    }

    @Override
    public String delete(int id) {
        boolean isDelete = bookRepository.delete(id);
        if (isDelete) {
            return "YOU HAVE DELETE SUCCESSFULLY!";
        } else {
            return "YOU CAN NOT DELETE!";
        }
    }

    @Override
    public BookDto update(int id, BookDto dto) {
        boolean isUpdated = bookRepository.update(id, dto);
        if (isUpdated) {
            return dto;
        } else {
            return null;
        }
    }

    @Override
    public BookDto findOne(int id) {
        return bookRepository.findOne(id);
    }

    @Override
    public int countAllBook() {
        return bookRepository.coutAllBook();
    }

    @Override
    public CategoryDto selectByCateId(int category_id) {
        return bookRepository.selectByCateId(category_id);
    }

//    @Override
//    public BookDto selectByTitle(String title) {
//        return bookRepository.selectByTitle(title);
//    }
//
//    @Override
//    public List<BookDto> selectByCateId(int id,Pagination pagination) {
//        return bookRepository.selectByCateId(id,pagination);
//    }
}
