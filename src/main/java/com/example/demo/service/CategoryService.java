package com.example.demo.service;

import com.example.demo.model.DataTransfer.CategoryDto;
import com.example.demo.pagination.Pagination;

import java.util.List;

public interface CategoryService {

    String delete(int id);

    CategoryDto insert(CategoryDto dto);

    CategoryDto update(int id, CategoryDto dto);

    List<CategoryDto> getAll(Pagination pagination);

    int countAllCategory();

}
