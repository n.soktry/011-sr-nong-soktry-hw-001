package com.example.demo.service;

import com.example.demo.model.DataTransfer.BookDto;
import com.example.demo.model.DataTransfer.CategoryDto;
import com.example.demo.pagination.Pagination;

import java.util.List;

public interface BookService {
//    List<BookDto> findAll();

    BookDto insert(BookDto dto);

    String delete(int id);

    BookDto update(int id, BookDto dto);

    BookDto findOne(int id);

    List<BookDto> getAll(Pagination pagination);

    int countAllBook();

    CategoryDto selectByCateId(int category_id);

//    BookDto selectByTitle(String title);
//
//    List<BookDto> selectByCateId(int id, Pagination pagination);


}
