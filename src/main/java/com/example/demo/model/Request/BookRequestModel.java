package com.example.demo.model.Request;

import com.example.demo.model.DataTransfer.CategoryDto;

public class BookRequestModel {

    private String title;
    private String author;
    private String description;
    private String thumbnail ;
    private CategoryDto categoryDto;

    public BookRequestModel(String title, String author, String description, String thumbnail, CategoryDto categoryDto) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.thumbnail = thumbnail;
        this.categoryDto = categoryDto;
    }

    @Override
    public String toString() {
        return "BookRequestModel{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", description='" + description + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", categoryDto=" + categoryDto +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public CategoryDto getCategoryDto() {
        return categoryDto;
    }

    public void setCategoryDto(CategoryDto categoryDto) {
        this.categoryDto = categoryDto;
    }

    public BookRequestModel(){}


}
