package com.example.demo.controller.file;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
public class FileController {

    @PostMapping("/upload")
    public String upLoadFile(@RequestParam("file") MultipartFile multipartFile){
        File file1 = new File("\\D:\\NONG-SOKTRY\\HRD\\Spring\\Spring Project\\HOMEWORK\\src\\main\\java\\com\\example\\demo\\Image\\"+multipartFile.getOriginalFilename());

        try {

            file1.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file1);
            fileOutputStream.write(multipartFile.getBytes());
            fileOutputStream.close();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return "localhost:8080/img/" + file1.getName();
    }
}
