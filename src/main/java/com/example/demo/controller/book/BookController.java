package com.example.demo.controller.book;

import com.example.demo.model.BaseApiRespone;
import com.example.demo.model.DataTransfer.BookDto;
import com.example.demo.model.Request.BookRequestModel;
import com.example.demo.model.Respone.BookResponeModel;
import com.example.demo.service.BookService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class BookController {

    private BookService bookService;

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @DeleteMapping("/book/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        return new ResponseEntity<>(bookService.delete(id), HttpStatus.OK);
    }

    @PostMapping("/book")
    public ResponseEntity<BaseApiRespone<BookResponeModel>> insert(@RequestBody BookRequestModel requestModel) {
        ModelMapper mapper = new ModelMapper();
        BaseApiRespone<BookResponeModel> baseApiRespone = new BaseApiRespone<>();
        BookResponeModel respondModel = mapper.map(requestModel, BookResponeModel.class);
        baseApiRespone.setMessage("you added successfully");
        baseApiRespone.setHttpStatus(HttpStatus.CREATED);
        baseApiRespone.setTimestamp(new Timestamp(System.currentTimeMillis()));
        baseApiRespone.setData(respondModel);

        return new ResponseEntity<>(baseApiRespone, HttpStatus.CREATED);
    }

    @PutMapping("/book/{id}")
    public ResponseEntity<BaseApiRespone<BookResponeModel>> update(@PathVariable int id, @RequestBody BookRequestModel requestModel) {
        ModelMapper mapper = new ModelMapper();
        BaseApiRespone<BookResponeModel> baseApiRespone = new BaseApiRespone<>();
        BookDto dto = mapper.map(requestModel, BookDto.class);
        BookResponeModel respondModel = mapper.map(bookService.update(id, dto), BookResponeModel.class);
        baseApiRespone.setMessage("you updated successfully");
        baseApiRespone.setHttpStatus(HttpStatus.OK);
        baseApiRespone.setData(respondModel);
        baseApiRespone.setTimestamp(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(baseApiRespone);
    }

    @RequestMapping(value = "/book", method = RequestMethod.GET)
    public Map<String, Object> findOne(@RequestParam int id) {
        Map<String, Object> objectMap = new HashMap<>();
        BookDto dto = bookService.findOne(id);
        if (dto == null) {
            objectMap.put("Message", "id Not Found");
            objectMap.put("Response Code", 404);
        } else {
            bookService.findOne(id);
            objectMap.put("data", dto);
            objectMap.put("Message", "found successfully");
            objectMap.put("Response Code", 200);
        }
        return objectMap;
    }

}
