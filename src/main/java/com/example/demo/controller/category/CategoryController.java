package com.example.demo.controller.category;

import com.example.demo.model.BaseApiRespone;
import com.example.demo.model.DataTransfer.CategoryDto;
import com.example.demo.model.Request.CategoryRequestModel;
import com.example.demo.model.Respone.CategoryResponeModel;
import com.example.demo.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.lang.reflect.Type;
import java.sql.Timestamp;

@RestController
public class CategoryController {
    public CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        return new ResponseEntity <>(categoryService.delete(id),HttpStatus.OK);
    }

    @PostMapping("/category")
    public ResponseEntity<BaseApiRespone<CategoryResponeModel>> insert(@RequestBody CategoryRequestModel requestModel){
        ModelMapper mapper=new ModelMapper();
        BaseApiRespone<CategoryResponeModel> baseApiRespone=new BaseApiRespone <>();
        CategoryDto categoryDto=mapper.map(requestModel,CategoryDto.class);
        CategoryResponeModel responeModel=mapper.map(requestModel, (Type) CategoryResponeModel.class);
        baseApiRespone.setMessage("you added successfully");
        baseApiRespone.setHttpStatus(HttpStatus.CREATED);
        baseApiRespone.setTimestamp(new Timestamp(System.currentTimeMillis()));
        baseApiRespone.setData(responeModel);
        return new ResponseEntity <>(baseApiRespone,HttpStatus.CREATED);
    }

    @PutMapping("/category/{id}")
    public ResponseEntity<BaseApiRespone<CategoryResponeModel>> update(@PathVariable int id,@RequestBody CategoryRequestModel requestModel){
        ModelMapper mapper=new ModelMapper();
        BaseApiRespone<CategoryResponeModel> baseApiRespone=new BaseApiRespone <>();
        CategoryDto dto=mapper.map(requestModel,CategoryDto.class);
        CategoryResponeModel responeModel=mapper.map(categoryService.update(id,dto),CategoryResponeModel.class);
        baseApiRespone.setMessage("you updated successfully");
        baseApiRespone.setHttpStatus(HttpStatus.OK);
        baseApiRespone.setData(responeModel);
        baseApiRespone.setTimestamp(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(baseApiRespone);
    }

}
