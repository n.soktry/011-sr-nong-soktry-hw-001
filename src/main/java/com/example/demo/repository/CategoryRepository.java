package com.example.demo.repository;

import com.example.demo.model.DataTransfer.CategoryDto;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;


@Repository
public interface CategoryRepository {

    @Delete("DELETE FROM tb_categories WHERE id=#{id}")
    boolean delete(int id);

    @Insert("INSERT INTO tb_categories (title) VALUES (#{title})")
    boolean insert(CategoryDto categoryDto);

    @Update("UPDATE tb_categories SET title=#{categoryDto.title} WHERE id=#{id}")
    boolean update(int id,CategoryDto categoryDto);

}
